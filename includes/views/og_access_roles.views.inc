<?php

/**
 * @file
 * Provides support for the Views module.
 */

/**
 * Implements hook_views_data().
 */
function og_access_roles_views_data() {
  // Basic table information.
  $data['og_access_roles']['table']['group']  = t('Organic groups');

  // Join to 'node' as a base table.
  $data['og_access_roles']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'role' => array(
      'left_table' => 'role',
      'left_field' => 'rid',
      'field' => 'rid',
    ),
  );

  // ----------------------------------------------------------------
  // Fields

  // Roles that have access to a node via og_access_roles
  $data['og_access_roles']['rid'] = array(
    'title' => t('Roles with access'),
    'help' => t('Roles that have access to a node via og_access_roles.'),
    'field' => array(
      'handler' => 'og_access_roles_handler_field_rid',
      'no group by' => TRUE,
    ),
    'filter' => array(
      'handler' => 'og_access_roles_handler_filter_user_roles',
      'numeric' => TRUE,
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_users_roles_rid',
      'name table' => 'role',
      'name field' => 'name',
      'empty field name' => t('No role'),
      'numeric' => TRUE,
    ),
  );

  return $data;
}
