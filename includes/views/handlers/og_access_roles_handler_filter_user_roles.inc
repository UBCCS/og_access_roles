<?php

/**
 * @file
 * Definition of og_access_roles_handler_filter_user_roles
 */

class og_access_roles_handler_filter_user_roles extends views_handler_filter_user_roles {

  /**
   * DRUPAL_AUTHENTICATED_RID was unset from the value_options array.
   * We want all the roles.
   *
   * @see views_handler_filter_user_roles
   */
  function get_value_options() {
    $this->value_options = user_roles();
  }

  /**
   * Override empty and not empty operator labels to be clearer.
   *
   * @see views_handler_filter_user_roles
   */
  function operators() {
    $operators = parent::operators();
    $operators['empty']['title'] = t("No roles specified via og_access_roles");
    $operators['not empty']['title'] = t("Any roles");
    return $operators;
  }
}
